# simpleForm.css - easy, responsive, simple.

## Introduction
simpleForm.css is a tiny library that allows you to create responsive web forms with clear UI.
simpleForm.css is written in CSS and doesn't require any additional library to function properly.

## Mobile First
simpleForm.css is designed by the 'Responsive Web Design' principles to make sure that every form, complex or not, will perfectly fit in every platform and screen's size.

## Clear UI with Plenty of Presets
simpleForm.css comes with bunch of built-in colors themes that can easily be initiate in your form according to your needs.
All the preset themes are designed to be easy to read with contrast color and still stays minimalized.

## Simple To Develop Complex Forms
simpleForm.css works with CSS grid and allows the developer to create many UI features: number of columns, size of the form (wide) etc.

## Get Started
### Installation
`simpleForm.css` doesn't require any special package/library, all you have to do is to add the link to the source under the `head` tag:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    ...
    <link rel="stylesheet" href="simpleForm.css"/>
    ...
</head>
...
```

Note: you can use a local library or a remote CDN file (*link coming soon*).

### Hello World!
Let's create our first form. Using simpleform.css is very easy;
To initialize `simpleform.css` on the desirable form just add `class="simpleform"` to the `form` element.

```html
<form class="simpleform" method="post">
    <h3>Welcome Friend! Please sign in</h3>
    <input type="text" placeholder="User Name">
    <input type="password" placeholder="password">
    <button type="submit">Sign in</button>
</form> 
```

now `simpleform.css` applies the default grey style on all `form` childes and gives the form support responsive design.

### Wider Forms For Wide Reasons
`simpleform.css` comes with three built-in sizes: `wide`, `wider` and `default` (no need to define).

Below the table shows how the form will react for each screen's size.

| Screen width | Class name | Relative width |
| ------------ | ---------- | -------------- |
| <520px       | default    | 85%            |
|              | wide       | 85%            |
|              | wider      | 85%            |
|520px<= <768px| default    | 60%            |
|              | wide       | 70%            |
|              | wider      | 80%            |
|768px<= <1200px| default   | 40%            |
|              | wide       | 50%            |
|              | wider      | 60%            |
| 1200px<=     | default    | 20%            |
|              | wide       | 30%            |
|              | wider      | 40%            |

On mobile the form will always be 80%.

In order to change the form width , pick the chosen width class name and add it after `simpleform`:

```html
<form class="simpleform wide" method="post">
...
</form> 
```

### Creating Complex Form
Now things getting more interesting, with super easy to use grid system `simpleform.css` will make sure that you'll create
forms with multi columns and different sizes.
#### How It works:
First, we have to add a new row to the form. This row will contain the columns soon.

To add a new row create a new `div` element with `sf-row` class:
```html
<div class="sf-row">
...
</div> 
``` 
#### Adding Columns:
simpleForm.css have different sizes columns, all of them are defined in percentages.
To add a new column create a `div` element with `sf-col-XX` (XX = the size in %).

| Class name | Width | Class name | Width |
| :--------: | :---: | :--------: | :---: |
| sf-col-25  | 25%   | sf-col-33  | 33.33%|
| sf-col-50  | 50%   | sf-col-66  | 66.66%|
| sf-col-75  | 75%   |            |       |


For example let's create a form with two rows: The first row will contain two columns with equal size, each of them will have text input inside.
The second row will contain two columns with two different sizes (25% and 75% of the form width), each of them will contain button inside.
```html
<form class="simpleform" method="post">
    <div class="sf-row">
        <div class="sf-col-50">
            <input type="text">
        </div>
        <div class="sf-col-50">
            <input type="text">
        </div>
    </div>
    <div class="sf-row">
        <div class="sf-col-75">
            <button>Show me more!</button>
        </div>
        <div class="sf-col-25">
            <button>Clear form</button>
        </div>
    </div>
</form> 
```
#### Adding Some Colors:
With 6 built-in color themes you can easily adjust the look of your form.
Specify the color name in the class and watch the results.
Available colors: 
- `red`
- `orange`
- `yellow`
- `green`
- `blue`
- `purple`