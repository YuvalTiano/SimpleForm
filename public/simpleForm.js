$(function () {
    // For future using
    var usingAnimateCSS = true;
    var currentColor = null;

    // The button
    // The first button with type=submit OR the first button
    var button = $(".simpleform button[type='submit']:first").add(".simpleform button:first");
    button.on("click", function () {
        validator(false);
    });

    // Removing the animated class when it's finish
    $(".simpleform").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
        $(this).removeClass("animated bounceInUp");

        // For some reason Animate.css doing some issue with input with autofocus attribute.
        $("#username").focus();
        $(".slider")
            .css("display", "block")
            .addClass("animated pulse");
    });

    // When user click on the "Not registered?" paragraph
    $("#notRegistered").click(function () {
        console.log("Not registered paragraph has clicked");
        $(this).css("display", "none");
        button.off().on("click", function () {
            validator(true);
        });
        button.off();
        button.fadeToggle(200).text("Sign up").fadeToggle(400);
        var gender = '<input type="radio" name="gender" value="male" checked> Male<input type="radio" name="gender" value="female"> Female';
        $("#username").before('<div class="sf-row"><div class="sf-col-50"><input type="text" placeholder="First Name"></div><div class="sf-col-50"><input type="text" placeholder="Last Name"></div><input type="email" placeholder="Email Address"><input id="birthday" type="text" placeholder="Birthday DD/MM/YYYY"></div>');
        $("#password").after('<input type="password" placeholder="Retype Password" minlength="8">' + gender);
        $("#birthday").focus(function () {
            $(this).attr("type", "date");
        });
        if (currentColor) {
            $(".simpleform").find("input").addClass(currentColor);
        }
        $("input:first").focus();
    });

    function validator(newUser) {
        var keepValidation = true;
        // Validate the form
        $(".badRequest:not(:last)").remove();
        console.log("Validator function has called");
        var username = $("#username").val();
        var password = $("#password").val();
        for (var i = 0; i <= $("input").length; i ++) {
            if ($("input").eq(i).val() == "") {
                console.log('Bad Request - ' + $("input").eq(i).attr("placeholder") + ' is missing');
                $("input").eq(i).addClass("animated shake").after('<P class="badRequest">' + $("input").eq(i).attr("placeholder")+' is missing</P>');
                $(".badRequest").css("display", "block");
                keepValidation = false;
            }
        }
        if (newUser) {
            // Compare the two password inputs
            var secondPassword = $("input[type='password']:last").val();
            var result = [];
            if (keepValidation && (password == secondPassword)) {
                $("input").each(function () {
                    console.log($(this).val());
                    result.push($(this).val());
                });
                // result.append($("input[name=gender]:checked").val());
                console.log("Great! welcome " + username);
                console.log(result);
            }

            else {
                $("#password").after('<P style="display: block" class="badRequest">Password don\'t match</P>');
                keepValidation = false;
            }

            // Remove the animated class after quate
            setTimeout(function () {
                $("input").removeClass("animated shake");
            }, 2000);
        }

        if (keepValidation) {
            ValidationSuccess(username);
        }
    }

    function ValidationSuccess(username) {
        // Vanish all the inputs after success validation.
        $(".badRequest").remove();
        $("input")
            .slideUp(800)
            .parent().css("min-height", "30vh");
        $("h1").text('Welcome ' + username + '!');
        // Renew the submit auto to hide the form when click
        button
            .text("Keep surfing")
            .off()
            .on("click", function () {
                $(this).parent().addClass("animated fadeOutUp");
                $(".slider").addClass("animated fadeOutDown");
                $(this).parent().one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                    $(this).css("display", "none");
                });
            });
    }

    $("#whoami").click(function() {
        $(".simpleform").animate({opacity:0} ,500);
        $(".slider").animate({opacity:0} ,500);
        $(".example-title")
            .text("Coming soon...")
            .animate({fontSize:"+=300%"} ,1500);
    });

    $(".slider .circle").on("click", function () {
            initColor($(this).attr("data-color"));
        });

    function initColor(color) {
        if (color === "random") {
        // ^ Useless for now
            // All the available colors
            var colorList = ["red", "orange", "yellow", "green", "blue", "purple"];
            // Pick a random color
            color = colorList[Math.floor(Math.random()*colorList.length)];
        }

        $(".simpleform").each(function () {
            if ($(this).hasClass("wide")) {
                $(this).removeClass().addClass("simpleform wide " + color);
            }
            else if ($(this).hasClass("wider")) {
                $(this).removeClass().addClass("simpleform wider " + color);
            }
            else {
                $(this).removeClass().addClass("simpleform " + color);
            }
            button.removeClass().addClass(color);
            $("input").removeClass().addClass(color);

            currentColor = color;
        });
    }

    // User define color theme
    var R = 0, G =0, B = 0, A =1 ;
    var previewCircle = $("#preview-circle");
    $("input[type='color'], input[type='range']").on("mousemove", function () {

        function hexColorToDec() {
            // Useless
            var hexColor = $("input[type='color']").val(); // #RRGGBB Base 16
            var transparency = parseFloat($("input[type='range']").val() / 10);
            var R = parseInt(hexColor.substring(1,3), 16);
            var G = parseInt(hexColor.substring(3,5), 16);
            var B = parseInt(hexColor.substring(5,7), 16);
        }

        switch ($(this).attr("name")) {
            case "R":
                R = $(this).val();
                break;
            case "G":
                G = $(this).val();
                break;
            case "B":
                B = $(this).val();
                break;
            case "A":
                A = $(this).val();
        }

        previewCircle.css("background-color", "rgba("+R+","+G+","+B+","+A+")");
        previewCircle.siblings("p").text("rgba("+R+","+G+","+B+","+A+")");
        $("h1").css("color", "rgba("+R+","+G+","+B+","+A+")");
        button.css("background-color", "rgba("+R+","+G+","+B+","+A+")");
    });
});