$(function () {
    // Set random color on click "Switch Color" button
    var color = ["red", "orange", "yellow", "green", "blue", "purple"];
   $("button").eq(-1).on("click", function () {
       var randomColor = color[Math.floor(Math.random()*color.length)];
       $("form").removeClass().addClass("simpleform " + randomColor).find("input, button").removeClass().addClass(randomColor);
       return false;
   });
   // Init fullpage.js
   $("#fullpage").fullpage(
       {verticalCentered:false}
   );
   var checkIfLastSectionIsActive = function() {
       setInterval(function () {
           if ($(".section:last-of-type").hasClass("active")) {
               $(".example-title").fadeOut(200).css("display", "none");
               $(".slider").fadeOut(600);
           }
           else if (!$(".section:last-of-type").hasClass("active") && $(".example-title").css("display") === "none") {
               $(".example-title").css("display", "block").fadeIn(200);
               $(".slider").fadeIn(400);
           }
       }, 300);
   };
    checkIfLastSectionIsActive();
});